function pathJoin(parts, sep){
    var separator = sep || '/';
    var replace   = new RegExp(separator+'{1,}', 'g');
    return parts.join(separator).replace(replace, separator);
}


Vue.component("attribute", {
    props: {
        dev: {
            type: String,
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        desc: {
            type: String,
            required: false,
            default: "",
        },
        get: {
            type: Boolean,
            required: true,
            default: false,
        },
        set: {
            type: Boolean,
            required: true,
            default: false,
        },
        type: {
            type: String,
            required: true,
        },
    },
    template: `
<div class="attribute">
<p><b>{{name}}</b></p>
<p v-if="desc"><i>{{desc}}</i></p>
<div v-if="data != null">
<hr>
<p>{{data}}</p>
</div>
<args v-if="get"
v-on:run-func="runFunc($event, 'get')"
name="get"></args>
<args v-if="set"
v-on:run-func="runFunc($event, 'set')"
name="set"></args>
</div>`,
    data() {
        return {
            data: null
        }
    },
    methods: {
        async runFunc(data, func) {
            call = "/api" + window.location.pathname + "?"
            call = call + "call=" + this.type + "&name=" + this.name + "&func=" + func
            for (d in data) {
                call = call + "&type" + d + "=" + data[d].type + "&data" + d + "=" + data[d].data;
            }
            remote = await (await fetch(call)).json();
            if (typeof remote.data == "undefined")
                this.data = null
            else
                this.data = remote.data
        }
    }
});

Vue.component("method", {
    props: {
        dev: {
            type: String,
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        desc: {
            type: String,
            required: false,
            default: "",
        },
    },
    template: `
<div class="method">
<p><b>{{name}}</b></p>
<p v-if="desc"><i>{{desc}}</i></p>
<div v-if="data">
<hr>
<p>{{data}}</p>
</div>
<args name="call"
v-on:run-func="runFunc($event)">
</args>
</div>`,
    data() {
        return {
            data: null
        }
    },
    methods: {
        async runFunc(data) {
            call = "/api" + window.location.pathname + "?"
            call = call + "call=method&name=" + this.name
            for (d in data) {
                call = call + "&type" + d + "=" + data[d].type + "&data" + d + "=" + data[d].data;
            }
            remote = await (await fetch(call)).json();
            this.data = remote.data
        }
    }
});

Vue.component("args", {
    props: {
        name: {
            type: String,
            required: true,
        },
    },
    template: `
<div class="args">
<div class="fbox">
<p>{{name}}</p>
<button v-on:click="runFunc()">Run</button>
<button v-on:click="addArg()">+</button>
</div>
<arg v-for="(a, index) in args" :key="a.index" :argindex="index"
v-on:remove-index="removeIndex($event)"
v-on:update-data="updateData($event)">
</arg>
</div>`,
    data() {
        return {
            args: []
        }
    },
    methods: {
        addArg() {
            this.args.push({index: Math.random(), type: "text", data: ""});
        },
        removeIndex(index) {
            this.args.splice(index, 1);
        },
        updateData(obj) {
            this.args[obj.index].type = obj.type;
            this.args[obj.index].data = obj.data;
        },
        runFunc() {
            this.$emit("run-func", this.args)
        }
    }
});

Vue.component("arg", {
    props: {
        argindex: {
            type: Number,
            required: true,
            default: 0,
        }
    },
    template: `
<div class="arg">
<div class="fbox">
<select v-model="type" v-on:click="updateData">
<option value="boolean">Boolean</option>
<option value="number">Number</option>
<option value="text">String</option>
<option value="bytearray">Byte Array</option>
</select>
<input v-model="data" v-on:input="updateData">
<button v-on:click="removeIndex()">-</button>
</div>
</div>`,
    data() {
        return {
            type: "text",
            data: "",
        }
    },
    methods: {
        removeIndex() {
            this.$emit("remove-index", this.argindex)
        },
        updateData() {
            this.$emit("update-data", {index: this.argindex, data: this.data, type: this.type})
        }
    }
});


var app = new Vue({
    el: '#app',
    data: {
        name: "",
        driver: "",
        methods: [],
        local_attrs: [],
        private_attrs: [],
        driver_attrs: [],
        children: [],
    },
    methods: {
        childPath(child) {
            return pathJoin([window.location.pathname, child]);
        }
    },
    computed: {},
    async beforeMount() {
        remote = await (await fetch("/api" + window.location.pathname)).json();
        this.name = remote.name;
        this.driver = remote.driver;
        this.methods = remote.methods;
        this.local_attrs = remote.local_attrs;
        this.private_attrs = remote.private_attrs;
        this.driver_attrs = remote.driver_attrs;
        this.children = remote.children;
    }
});
